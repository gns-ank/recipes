# History

This recipe is from my grandmother, who found it in some newspaper.

# Ingredients and tools

## Pastry (puff pastry)

 * 250g butter, cold
 * 250g Crème fraîche, cold
 * 500g 00 Italian type flour, sieved
 * pinch of salt, sieved

## Filling 1

 * 150g of cranberries, blended.

## Filling 2 (variant 1)

 * 200g butter, room temperature
 * 40g of powdered sugar
 * 200--250g of condensed milk, room temperature
 * juice of ½ lemon

## Filling 2 (variant 2) - untested

 * 250g butter
 * 45--50g flour
 * 200-250ml milk (what about in grams?)
 * 150--175g sugar
 * 2 egg yolks
 * Some form of vanilla

## Various tools which this recipe uses:

 * 22cm diameter round cake tin (you can use larger, but will need to adjust the numbers).
   If your tin is, say, 30cm, multiply all quantities by (30/22)^2.

# Method

## Making the pastry

1. Sieve the flour and salt and make a well in the middle.
2. Use a grater to grate cold butter into the well (easier to do if the stick of butter is rolled into the sieved flour a few times).
3. Add Crème fraîche
4. Use a long knife and chop it until all the flour is combined.
5. form a long cylinder and wrap it into cling film.
6. Leave at least for 3--4 hours to chill in the fridge.

## Baking the sheets

1. For one sheet you should use about 140g of dough. Just slice of the right amount from the cylinder and start rolling. For more consistent results, keep part of the dough in the fridge.
2. Once the rolled dough sheet is large enough, put the bottom of the cake tin and cut around by using a sharp knife. Then use a fork to make lots of small holes, which will imped raising of the sheet.
3. Use a rolling pin to place the rolled sheet onto a hot baking tray lined with baking parchment.
4. In order to imped raising of the sheet, you can also experiment with sandwiching the sheets between two baking sheets, but this is entirely optional.

5. With the given ingredients you should be able to make about 6-7 sheets. It takes around 15 minutes for one sheet to bake in an 170°C oven (with fan).
6. Since this takes a while, you can start preparing ingredients for the fillings and even do the fillings.
7. When you take out the baked sheets, leave them to rest on a rack.
8. Also, bake any cut-offs as they will be needed for decorating.

## Making the filling (cranberries)

Since this cake is quite rich and sweet, blended cranberries is a must as they cut through the richness and sweetness.
You can also experiment with making a cranberry sauce, by just boiling them in a small amount of water and then reducing the mixture, but I prefer fresh cranberries instead.

1. Just blend them with a hand blender or whatever you have.
2. Set the cranberries aside

## Making the filling (variant 1)

1. Use a hand mixer to fluff the butter up and once it has incorporated a bit of air and has softened considerably proceed with the next steps.
2. Add powdered sugar in batches.
3. Afterwards, add condensed milk, also in batches.
4. Lastly, add the lemon juice, in batches.
5. You should end up with a fluffy buttery mixture which should be easy to spread.

## Making the filling (variant 2)

1. Melt the butter and after it starts to froth, add flour.
2. Separately heat milk with sugar until it's dissolved.
3. Then combine both liquids and mix whilst heating until it thickens (probably, just coating the spoon).
4. Cool the mixture and add vanilla.
5. Add egg yolks one by one whilst stirring so that you don't get scrambled egg.
6. Mix it further until it becomes fluffy and airy.

## Assembling

1. From all the sheets choose two which will be the top and the bottom.
 Grab a nice flat plate onto which you will assemble the cake.
2. Take one and spread the filling and repeat it until you have got two sheets left.
3. Then put one of the two sheets, use cranberies and put the last sheet on top.
4. You should have about 1/5 of the initial filling mixture left and cover with it the whole torte --- the sides and the top.
5. Crumble the off-cuts with your hands and stick them to the outside of the cake.
6. Once done, put the loose base of the cake tin on top of the cake and put the side of the tin around.
7. Add some weighty thing on top of loose cake tin base so that the layers and the cake is compressed.
8. Leave overnight.

# Notes
