Mano mamos tortas, kurį darydavo man per kiekvieną gimtadienį.

# Ingredientai

## Blynai

 * 40 g sviesto
 * 100 g cukraus
 * 40 g medaus
 * 5 g malto cinamono
 * 5 g maltų gvazdikėlių
 * 2 g kepimo miltelių
 * 2 kiaušiniai (110g)
 * 2 g vaniles ekstrakto
 * 450 g miltų

   7-8 blynai, 22 cm

## Kremas

 * 1000 g creme fraiche
 * 100g cukraus
 * sultys iš vienos citrinos

## Tarp sluoksniu

 * riekeles apelsinu
 * sokoladas ant virsaus

# Metodas

## Lakštai

1. Ištirpinti sviestą ir atšaldyti.
2. Sudėti cukrų ir kiaušinius ir viską išplakti iki vientisos masės.
3. Sudėti medų ir prieskonius ir pastoviai maišant kaitinti ant vandens garų (Baine marie).
4. Kai sutirštėja, nukelti, sudėti kepimo miltelius ir pradėti dėti miltus. Dėti miltus po maždaug 75 gramus juos visada išsijojant ir maišyti.
5. Kai pasidaro sunku maišyti, išsijoti likusius miltus ant švaraus paviršiaus ir pradėti minkyti rankomis.

6. Kai tešla išminkyta, suformuoti pailgą cilindrą ir suvinioti į maistinę plievelę.
7. Tešlai sukietėjus padalinti tešlą į 8 lygias dalis ir jas suvinioti į plievele ir sudėti atgal į šaldytuvą.
8. Imti po vieną gabaliuka, iškočioti, apipjauti padėjus skardos padą ir viską (ir atraižas) 180°C (su konvekcijos/propelerio funkcija), 7 minutes. Lakštus atvėsinti.

## Kremas ir pertepimas

1. Sudėti viskė į didelį indą ir plakti kol pasidaro standi masė (truputį skystesnė nei plakta grietinėlė).

## Torto darymas

1. Pasiimti du gražiausius lakštus ir atsidėti vieną pagrindui, kitą --- viršui.
2. Pertepti kremu ir į kas antrą dėti labai plonas (3--5 mm) apelsinų riekeles.
3. Ant viršutinio lakšto uždėti maždaug du kartus daugiau kremo nei tarp sluoksnių.
4. Palikti šaldytuve per naktį.
5. Atraižas sumalti ir ryte trupinius panaudoti dekoracijai.

